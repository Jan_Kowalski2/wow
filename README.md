﻿# Wow
*Webpack based utility made for creating static HTML websites using SCSS and Babel tools.* 

## How to start
It's really simple. You just need to type in console `npm install` and the project will be ready. 
For most cases you will need to use two commands:

 - `npm start` is used for development. Webpack will initialize local server. When you change code inside `src/`
folder, website will refresh.
 - `npm run build` is used for production. The `dist/` folder will be made where you will find minified, transpilled files. More information you will find in "Production" section.

## Development
The `src/` folder is for development. It's structure is very simple:

    _js/
	    app.js
	_scss/
		style.scss
		... SCSS files
	img/
	index.html
	index.js
	.babelrc

 
|Folder or file name |Description|
|--|--|
|_js|Core Javascript folder. You can create ES6 Javascript files and import them to app.js file, but of course you can modify the structure of this folder.  |
|_scss|SCSS files folder. All style files are here.|
|style.scss and other files|In style.scss you will insert imports to other SCSS files. It's the main style file which holds references to other SCSS files. **It is required** to use SCSS files - webpack is watching only for them.
|img/|Images folder. Simple.|
|index.html|Static website HTML file. Create other HTML files next to this file.|
|index.js|For clarity, this JS file holds references to _js/app.js file and CSS file.|
|.babelrc|Babel configuration file.|

> For **clarity**, it is good practice to create SCSS files in `_scss` folder and JS files in `_js` folder.

### A word about CSS
You can write pure CSS in SCSS files and create your own. I have made `components/` folder inside `_scss/` folder because in my opinion it's good practice to create style files for website sections there. File names of .scss files explain theirselves.

   `postcss.config.js` file in `_scss` folder is responsible for autoprefixer configuration. It adds prefixes for older browsers. The setup is in `package.json`  file:
   
    browserslist": [">1%","last 2 versions"]

 
### ...and about JS
Like in React, the Javascript files are just modules, which can be exported to any file. I propose to import modules to `app.js` file.

## Publication
After typing `npm run build` webpack will create `dist/` folder in main catalogue and do some work:

 - Bigger images will be optimized,
 - Smaller ones will be inlined inside the HTML as a Base64 URL,
 - `main.css` file will be made - it contains SCSS merged files translated to CSS, minified and with prefixes,
 - `main.js` file will be created - it is `app.js` transpilled to ES5 Javascript.
 - `index.html / HTML files` will be added and minified.

## Footer

Created using tutorial from [www.valentinog.com](https://www.valentinog.com/blog/from-gulp-to-webpack-4-tutorial/#Switching_from_Gulp_to_webpack_compiling_SASS_to_CSS_adding_vendor_prefixes_to_CSS)

Created by [Piotr Kwiatek](https://bitbucket.org/Piotr_Kwiatek/) 